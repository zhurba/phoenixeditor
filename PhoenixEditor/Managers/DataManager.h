#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H

#include <string>
#include <memory>

#include "DataTypes.h"
#include "Notifier.h"

using CatalogNode = TreeNode<CatalogData>;

class DataManager
{
public:
    ~DataManager();
    static DataManager* Instance();

public:
    void LoadData();
    void SaveData();

    void LoadActiveData();
    void SaveActiveData();

    std::shared_ptr<CatalogNode> CreateNode(const CatalogData& data, std::shared_ptr<CatalogNode> parent);
    std::shared_ptr<CatalogNode> FindNode(const std::wstring& data) const;
    void DeleteNode(std::shared_ptr<CatalogNode> node);
    void ClearData();
    void MoveNodeUp(std::shared_ptr<CatalogNode> node);
    void MoveNodeDown(std::shared_ptr<CatalogNode> node);

    std::shared_ptr<CatalogNode> GetRootNode() const;
    void SetRootNode(const std::shared_ptr<CatalogNode> rootNode);

    std::shared_ptr<CatalogNode> GetActiveNode() const;
    void SetActiveNode(const std::shared_ptr<CatalogNode> activeNode);

    const std::string& GetFileName() const;
    void SetFileName(const std::string& fileName);

    Sender* GetActiveNodeChanged();

private:
    DataManager();
    DataManager(const DataManager&) = delete;
    DataManager& operator=(const DataManager&) = delete;

private:
    std::string m_fileName;
    std::shared_ptr<CatalogNode> m_spRootNode;
    std::shared_ptr<CatalogNode> m_spActiveNode;
    Sender m_activeNodeChanged;
};

#endif
