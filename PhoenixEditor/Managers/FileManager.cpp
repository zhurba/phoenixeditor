#include <fstream>

#include "FileManager.h"
#include "Strings.h"

FileManager::~FileManager()
{
}

FileManager* FileManager::Instance()
{
    static FileManager fileManager;
    return &fileManager;
}

void FileManager::DeSerializeRecentFiles()
{
    m_recentFiles.clear();
    std::ifstream ifs(strConfigFilePath);
    if (ifs.is_open())
    {
        while (!ifs.eof())
        {
            m_recentFiles.emplace_back();
            std::getline(ifs, m_recentFiles.back());
        }

        ifs.close();
    }
}

void FileManager::SerializeRecentFiles()
{
    std::ofstream ofs(strConfigFilePath, std::ofstream::out);
    bool isFirstFile = true;
    for (auto& fileName : m_recentFiles)
    {
        if (isFirstFile)
        {
            isFirstFile = false;
        }
        else
        {
            ofs << std::endl;
        }
        ofs << fileName;
    }
    ofs.close();
}

const std::list<std::string>& FileManager::GetRecentFiles()
{
    return m_recentFiles;
}

void FileManager::AddRecentFile(const std::string& newFileName)
{
    if (m_recentFiles.empty() || newFileName != m_recentFiles.front())
    {
        m_recentFiles.push_front(newFileName);
        const int numRecentFiles = 4;
        if (m_recentFiles.size() > numRecentFiles)
        {
            m_recentFiles.pop_back();
        }
        SerializeRecentFiles();
    }
}

FileManager::FileManager()
{
    DeSerializeRecentFiles();
}