#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <list>
#include <string>

class FileManager
{
public:
    ~FileManager();
    static FileManager* Instance();

    void DeSerializeRecentFiles();
    void SerializeRecentFiles();
    const std::list<std::string>& GetRecentFiles();
    void AddRecentFile(const std::string& newFileName);

private:
    FileManager();
    FileManager(const FileManager&) = delete;
    FileManager& operator=(const FileManager&) = delete;

private:
    std::list<std::string> m_recentFiles;
};

#endif