#include <queue>

#include "DataManager.h"

DataManager::~DataManager()
{
    ClearData();
}

DataManager* DataManager::Instance()
{
    static DataManager dataManager;
    return &dataManager;
}

void DataManager::LoadData()
{
}

void DataManager::SaveData()
{
}

void DataManager::LoadActiveData()
{
}

void DataManager::SaveActiveData()
{
}

std::shared_ptr<CatalogNode> DataManager::CreateNode(const CatalogData& data, std::shared_ptr<CatalogNode> parent)
{
    std::shared_ptr<CatalogNode> treeNode = std::make_shared<CatalogNode>(data);

    if (parent.get())
    {
        parent->AddChild(treeNode);
    }
    else
    {
        SetRootNode(treeNode);
    }

    //std::function<CatalogNode::FuncType> func = std::bind(&DataManager::OnDataChanged, this, std::placeholders::_1);
    //treeNode.RegisterReceiver(this, func);

    return treeNode;
}

std::shared_ptr<CatalogNode> DataManager::FindNode(const std::wstring& data) const
{
    std::shared_ptr<CatalogNode> currentNode;
    std::queue<std::shared_ptr<CatalogNode>> nodes;
    nodes.push(m_spRootNode);
    while (!nodes.empty())
    {
        currentNode = nodes.front();
        nodes.pop();
        if (currentNode->GetData().GetTitle() == data)
            return currentNode;

        for (currentNode->Init(); auto child = currentNode->Next();)
        {
            nodes.push(child);
        }
    }

    return std::shared_ptr<CatalogNode>();
}

void DataManager::DeleteNode(std::shared_ptr<CatalogNode> node)
{
    if (node == m_spActiveNode)
    {
        SetActiveNode(nullptr);
    }
    CatalogNode* parent = node->GetParent();
    if (parent)
    {
        parent->RemoveChild(node);
    }
    else
    {
        SetRootNode(nullptr);
    }
}

void DataManager::ClearData()
{
    SetActiveNode(nullptr);
    SetRootNode(nullptr);
}

void DataManager::MoveNodeUp(std::shared_ptr<CatalogNode> node)
{
    CatalogNode* parent = node->GetParent();
    if (parent)
    {
        parent->MoveChild(node, false);
    }
}

void DataManager::MoveNodeDown(std::shared_ptr<CatalogNode> node)
{
    CatalogNode* parent = node->GetParent();
    if (parent)
    {
        parent->MoveChild(node, true);
    }
}

std::shared_ptr<CatalogNode> DataManager::GetRootNode() const
{
    return m_spRootNode;
}

void DataManager::SetRootNode(const std::shared_ptr<CatalogNode> rootNode)
{
    m_spRootNode = rootNode;
}

std::shared_ptr<CatalogNode> DataManager::GetActiveNode() const
{
    return m_spActiveNode;
}

void DataManager::SetActiveNode(const std::shared_ptr<CatalogNode> activeNode)
{
    m_spActiveNode = activeNode;

    SenderParams<std::shared_ptr<CatalogNode>> params(activeNode);
    m_activeNodeChanged.Notify(&params);
}

const std::string& DataManager::GetFileName() const
{
    return m_fileName;
}

void DataManager::SetFileName(const std::string& fileName)
{
    m_fileName = fileName;
}

Sender* DataManager::GetActiveNodeChanged()
{
    return &m_activeNodeChanged;
}

DataManager::DataManager()
    : m_fileName()
    , m_spRootNode(nullptr)
    , m_spActiveNode(nullptr)
{
}
