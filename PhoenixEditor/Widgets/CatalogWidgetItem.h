#ifndef CATALOG_WIDGET_ITEM_H
#define CATALOG_WIDGET_ITEM_H

#include <memory>
#include <QtWidgets/QTreeWidget>

#include "DataTypes.h"

class CatalogWidgetItem : public QTreeWidgetItem
{
public:
    CatalogWidgetItem(QTreeWidgetItem* parent, std::shared_ptr<CatalogNode> spData);
    virtual ~CatalogWidgetItem();

    void Activate() const;
    void SetTitle(int column);

protected:
    void OnDataChanged(const CatalogData& data);

private:
    void Init();

private:
    std::shared_ptr<CatalogNode> m_spData;
    std::shared_ptr<CatalogNode::FuncType> m_spReceiver;
};

#endif
