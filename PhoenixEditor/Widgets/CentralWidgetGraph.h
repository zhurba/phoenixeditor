#ifndef CENTRAL_WIDGET_GRAPH_H
#define CENTRAL_WIDGET_GRAPH_H

#include "CentralWidgetBase.h"

class RenderAreaWidget;

class CentralWidgetGraph : public CentralWidgetBase
{
    Q_OBJECT

public:
    CentralWidgetGraph(QWidget* parent = nullptr);
    virtual ~CentralWidgetGraph();

public:
    ECatalogDataType GetType() const override;

private:
    void Init();

private:
    RenderAreaWidget* m_renderAreaWidget;
};

#endif
