#ifndef RENDER_AREA_WIDGET_H
#define RENDER_AREA_WIDGET_H

#include <QtWidgets/QWidget>

#include "DataTypes.h"
#include "IconWidgetBase.h"

class QGraphicsView;
class QGraphicsScene;

class RenderAreaWidget : public QWidget
{
    Q_OBJECT

public:
    RenderAreaWidget(QWidget* parent = nullptr);
    virtual ~RenderAreaWidget();

public:
    void Init();
    void AddIconWidget(std::shared_ptr<CatalogNode> spNode);

private:
    QGraphicsView* m_gView;
    QGraphicsScene* m_gScene;
    std::vector<std::shared_ptr<IconWidgetBase>> m_iconWidgets;
};

#endif
