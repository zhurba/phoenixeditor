#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>

#include "CentralWidgetBase.h"
#include "DataUtils.h"

CentralWidgetBase::CentralWidgetBase(QWidget* parent)
    : QWidget(parent)
    , m_baseLayout(nullptr)
    , m_titleLabel(nullptr)
{
    Init();
}

CentralWidgetBase::~CentralWidgetBase()
{
}

void CentralWidgetBase::SetTitle(const std::wstring& title)
{
    if (m_titleLabel)
    {
        m_titleLabel->setText(DataUtils::WStringToQString(title));
    }
}

ECatalogDataType CentralWidgetBase::GetType() const
{
    return ECatalogDataType::None;
}

void CentralWidgetBase::Init()
{
    m_baseLayout = new QGridLayout(this);

    m_titleLabel = new QLabel;
    m_baseLayout->addWidget(m_titleLabel, 0, 0);
}
