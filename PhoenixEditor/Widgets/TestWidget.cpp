#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton.h>

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtGui/QPainter>

#include "TestWidget.h"


TestWidget::TestWidget(QWidget* parent)
    : QWidget(parent)
{
    Init();
}

TestWidget::~TestWidget()
{
}

void TestWidget::Init()
{
    setWindowTitle(QObject::tr("Hello, World!"));
    //setMinimumSize(400, 200);

    QGridLayout* grid = new QGridLayout(this);

    QLabel* label = new QLabel(QObject::tr("My label"), this);
    //label->setGeometry(20, 20, 80, 20);

    QLineEdit* lineEdit = new QLineEdit(this);
    //lineEdit->setGeometry(110, 20, 80, 20);

    QPushButton* button = new QPushButton(QObject::tr("My button"), this);
    //button->setGeometry(200, 20, 80, 20);

    grid->addWidget(label, 0, 0);
    grid->addWidget(lineEdit, 1, 1, 1, 2);
    grid->addWidget(button, 0, 2);
}

void TestWidget::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);

    painter.setPen(QPen(QBrush(Qt::red), 5, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin));

    painter.setFont(QFont("Arial", 30));
    painter.drawText(rect(), Qt::AlignCenter, "Qt");

    painter.drawLine(QPoint(0, 0), QPoint(width(), height()));

}