#ifndef CENTRAL_WIDGET_BASE_H
#define CENTRAL_WIDGET_BASE_H

#include <QtWidgets/QWidget>

#include "EnumTypes.h"

class QGridLayout;
class QLabel;

class CentralWidgetBase : public QWidget
{
    Q_OBJECT

public:
    CentralWidgetBase(QWidget* parent = 0);
    virtual ~CentralWidgetBase();

public:
    void SetTitle(const std::wstring& title);
    virtual ECatalogDataType GetType() const;

private:
    void Init();

protected:
    QGridLayout* m_baseLayout;

private:
    QLabel* m_titleLabel;
};

#endif
