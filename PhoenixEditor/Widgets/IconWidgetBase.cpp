#include <QtGui/QEvent.h>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QGraphicsProxyWidget>

#include "IconWidgetBase.h"
#include "DataUtils.h"

IconWidgetBase::IconWidgetBase(QWidget* parent, std::shared_ptr<CatalogNode> spData)
    : QWidget(parent)
    , m_spData(spData)
    , m_spReceiver(nullptr)
    , m_gpWidget(nullptr)
    , m_leTitle(nullptr)
    , m_posX(0)
    , m_posY(0)
{
    m_spReceiver = std::make_shared<CatalogNode::FuncType>(std::bind(&IconWidgetBase::OnDataChanged, this, std::placeholders::_1));
    m_spData->RegisterReceiver(m_spReceiver);

    Init();
}

IconWidgetBase::~IconWidgetBase()
{
}

void IconWidgetBase::SetProxyWidget(QGraphicsProxyWidget* gpWidget)
{
    m_gpWidget = gpWidget;
}

void IconWidgetBase::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton && m_gpWidget)
    {
        m_posX = event->pos().x();
        m_posY = event->pos().y();
    }
    else
    {
        QWidget::mousePressEvent(event);
    }
}

void IconWidgetBase::mouseMoveEvent(QMouseEvent* event)
{
    if ((event->buttons() & Qt::LeftButton) && m_gpWidget)
    {
        int oldPosX = m_gpWidget->pos().x();
        int oldPosY = m_gpWidget->pos().y();
        m_gpWidget->setPos(oldPosX + event->pos().x() - m_posX, oldPosY + event->pos().y() - m_posY);
    }
    else
    {
        QWidget::mouseMoveEvent(event);
    }
}

void IconWidgetBase::OnDataChanged(const CatalogData& data)
{
    Update();
}

void IconWidgetBase::OnTextChanged(const QString& text)
{
    std::wstring title = DataUtils::QStringToWString(text);

    CatalogData newData(m_spData->GetData());
    newData.SetTitle(title);

    m_spData->UnregisterReceiver(m_spReceiver);
    m_spData->SetData(newData);
    m_spData->RegisterReceiver(m_spReceiver);
}

void IconWidgetBase::Init()
{
    setMinimumSize(20, 20);

    QVBoxLayout* layout = new QVBoxLayout(this);

    m_leTitle = new QLineEdit;
    layout->addWidget(m_leTitle);

    QRadioButton* rbLink = new QRadioButton("Link");
    layout->addWidget(rbLink);

    Update();

    auto connection = QObject::connect(m_leTitle, SIGNAL(textChanged(const QString&)), this, SLOT(OnTextChanged(const QString&)));
}

void IconWidgetBase::Update()
{
    bool blocked = m_leTitle->blockSignals(true);
    m_leTitle->setText(DataUtils::WStringToQString(m_spData->GetData().GetTitle()));
    m_leTitle->blockSignals(blocked);
}
