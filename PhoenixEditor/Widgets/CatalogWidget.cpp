#include <QtCore/QObject>
#include <QtGui/QContextMenuEvent>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QMenu>

#include "CatalogWidgetItem.h"
#include "CatalogWidget.h"
#include "DataManager.h"
#include "Constants.h"
#include "Strings.h"
#include "DataUtils.h"

CatalogWidget::CatalogWidget(QWidget* parent)
    : QWidget(parent)
    , m_pbAddItem(nullptr)
    , m_pbDeleteItem(nullptr)
    , m_tree(nullptr)
    , m_contextMenu(nullptr)
    , m_spReceiver(nullptr)
{
    Init();
}

CatalogWidget::~CatalogWidget()
{
    DataManager::Instance()->GetActiveNodeChanged()->UnregisterReceiver(m_spReceiver);
}

void CatalogWidget::UpdateCatalog()
{
    std::shared_ptr<CatalogNode> rootNode = DataManager::Instance()->GetRootNode();
    CatalogWidgetItem* rootItem = rootNode ? CreateItem(rootNode, nullptr) : nullptr;

    m_pbAddItem->setEnabled(false);
    m_pbDeleteItem->setEnabled(false);

    m_tree->clear();
    m_tree->addTopLevelItem(rootItem);
    m_tree->expandAll();
}

void CatalogWidget::OnActiveNodeChanged(const SenderParamsBase* params)
{
    if (auto cnParams = dynamic_cast<const SenderParams<std::shared_ptr<CatalogNode>>*>(params))
    {
        const bool isAddItemVisible = (cnParams->m_arg1 != nullptr);
        m_pbAddItem->setEnabled(isAddItemVisible);
        m_pbDeleteItem->setEnabled(isAddItemVisible);
    }
}

void CatalogWidget::contextMenuEvent(QContextMenuEvent* event)
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    std::shared_ptr<CatalogNode> rootNode = DataManager::Instance()->GetRootNode();
    if (activeNode || !rootNode)
    {
        m_contextMenu->exec(event->globalPos());
    }
}

void CatalogWidget::OnItemPressed(QTreeWidgetItem* item, int column)
{
    CatalogWidgetItem* catalogWidgetItem = dynamic_cast<CatalogWidgetItem*>(item);
    catalogWidgetItem->Activate();
}

void CatalogWidget::OnItemChanged(QTreeWidgetItem* item, int column)
{
    CatalogWidgetItem* catalogWidgetItem = dynamic_cast<CatalogWidgetItem*>(item);
    catalogWidgetItem->SetTitle(column);
}

void CatalogWidget::OnItemAdd()
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    std::shared_ptr<CatalogNode> rootNode = DataManager::Instance()->GetRootNode();
    if (activeNode || !rootNode)
    {
        DataManager::Instance()->SetActiveNode(nullptr);
        CatalogData data(strNewCatalogItemName, strNewCatalogItemTitle);
        std::shared_ptr<CatalogNode> newNode = DataManager::Instance()->CreateNode(data, activeNode);
        UpdateCatalog();
    }
}

void CatalogWidget::OnItemDelete()
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DataManager::Instance()->DeleteNode(activeNode);
        UpdateCatalog();
    }
}

void CatalogWidget::OnItemMoveUp(bool checked)
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DataManager::Instance()->MoveNodeUp(activeNode);
        UpdateCatalog();
    }
}

void CatalogWidget::OnItemMoveDown(bool checked)
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DataManager::Instance()->MoveNodeDown(activeNode);
        UpdateCatalog();
    }
}

void CatalogWidget::Init()
{
    setMinimumSize(QSize(MIN_CATALOG_WINDOW_WIDTH, MIN_CATALOG_WINDOW_HEIGHT));

    QVBoxLayout* vbLayout = new QVBoxLayout(this);

    QHBoxLayout* hbLayout = new QHBoxLayout();
    {
        m_pbAddItem = new QPushButton(DataUtils::StringToQString(strAddContextItem));
        auto connectionAddItem = QObject::connect(m_pbAddItem, SIGNAL(clicked()), this, SLOT(OnItemAdd()));
        hbLayout->addWidget(m_pbAddItem);

        m_pbDeleteItem = new QPushButton(DataUtils::StringToQString(strDeleteContextItem));
        auto connectionDeleteItem = QObject::connect(m_pbDeleteItem, SIGNAL(clicked()), this, SLOT(OnItemDelete()));
        hbLayout->addWidget(m_pbDeleteItem);
    }
    vbLayout->addLayout(hbLayout);

    m_tree = new QTreeWidget;
    m_tree->setHeaderHidden(true);
    auto connectionItemPressed = QObject::connect(m_tree, SIGNAL(itemPressed(QTreeWidgetItem*, int)), this, SLOT(OnItemPressed(QTreeWidgetItem*, int)));
    auto connectionItemChanged = QObject::connect(m_tree, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SLOT(OnItemChanged(QTreeWidgetItem*, int)));
    vbLayout->addWidget(m_tree);

    UpdateCatalog();

    InitContextMenu();

    m_spReceiver = std::make_shared<Receiver<CatalogWidget>>(this, &CatalogWidget::OnActiveNodeChanged);
    DataManager::Instance()->GetActiveNodeChanged()->RegisterReceiver(m_spReceiver);
}

void CatalogWidget::InitContextMenu()
{
    QAction* actionAdd = new QAction(DataUtils::StringToQString(strAddContextItem), this);
    auto connectionItemAdd = QObject::connect(actionAdd, SIGNAL(triggered(bool)), this, SLOT(OnItemAdd()));

    QAction* actionDelete = new QAction(DataUtils::StringToQString(strDeleteContextItem), this);
    auto connectionItemDelete = QObject::connect(actionDelete, SIGNAL(triggered(bool)), this, SLOT(OnItemDelete()));

    QAction* actionMoveUp = new QAction("Move Up", this);
    auto connectionItemMoveUp = QObject::connect(actionMoveUp, SIGNAL(triggered(bool)), this, SLOT(OnItemMoveUp(bool)));

    QAction* actionMoveDown = new QAction("Move Down", this);
    auto connectionItemMoveDown = QObject::connect(actionMoveDown, SIGNAL(triggered(bool)), this, SLOT(OnItemMoveDown(bool)));

    m_contextMenu = new QMenu(this);
    m_contextMenu->addAction(actionAdd);
    m_contextMenu->addAction(actionDelete);
    m_contextMenu->addAction(actionMoveUp);
    m_contextMenu->addAction(actionMoveDown);
}

CatalogWidgetItem* CatalogWidget::CreateItem(std::shared_ptr<CatalogNode> spNode, CatalogWidgetItem* parent)
{
    CatalogWidgetItem* item = new CatalogWidgetItem(parent, spNode);
    item->setFlags(item->flags() | Qt::ItemIsEditable);

    for (spNode->Init(); auto child = spNode->Next();)
    {
        CreateItem(child, item);
    }

    return item;
}
