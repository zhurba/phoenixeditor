#ifndef NODE_WIDGET_H
#define NODE_WIDGET_H

#include <QtWidgets/QDockWidget>

class QPushButton;
class QLineEdit;
class QGridLayout;

class NodeWidget : public QDockWidget
{
    Q_OBJECT

public:
    NodeWidget(QWidget* parent = nullptr);
    virtual ~NodeWidget();

private Q_SLOTS:
    void OnAdd();

private:
    void Init();

private:
    std::map<QPushButton*, QLineEdit*> m_itemMap;
    QGridLayout* m_gridLayout;
    QPushButton* m_buttonAdd;
};

#endif
