#include <QtWidgets/QGridLayout>

#include "CentralWidgetGraph.h"
#include "RenderAreaWidget.h"
#include "DataManager.h"

CentralWidgetGraph::CentralWidgetGraph(QWidget* parent)
    : CentralWidgetBase(parent)
    , m_renderAreaWidget(nullptr)
{
    Init();
}

CentralWidgetGraph::~CentralWidgetGraph()
{
    m_baseLayout->removeWidget(m_renderAreaWidget);
    delete m_renderAreaWidget;
}

ECatalogDataType CentralWidgetGraph::GetType() const
{
    return ECatalogDataType::Graph;
}

void CentralWidgetGraph::Init()
{
    m_renderAreaWidget = new RenderAreaWidget;
    m_baseLayout->addWidget(m_renderAreaWidget, 1, 0);

    // Sample code:
    std::shared_ptr<CatalogNode> node = DataManager::Instance()->FindNode(L"�������");
    if (node)
    {
        m_renderAreaWidget->AddIconWidget(node);
    }
}
