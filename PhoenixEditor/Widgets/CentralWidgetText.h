#ifndef CENTRAL_WIDGET_TEXT_H
#define CENTRAL_WIDGET_TEXT_H

#include "CentralWidgetBase.h"

class RenderAreaWidget;

class CentralWidgetText : public CentralWidgetBase
{
    Q_OBJECT

public:
    CentralWidgetText(QWidget* parent = nullptr);
    virtual ~CentralWidgetText();

public:
    ECatalogDataType GetType() const override;

private:
    void Init();
};

#endif
