#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLineEdit>

#include "NodeWidget.h"
#include "Strings.h"
#include "DataUtils.h"

NodeWidget::NodeWidget(QWidget* parent)
    : QDockWidget(parent)
    , m_gridLayout(nullptr)
    , m_buttonAdd(nullptr)
{
    Init();
}

NodeWidget::~NodeWidget()
{
}

void NodeWidget::OnAdd()
{
    QPushButton* buttonDelete = new QPushButton;
    QPixmap pixmap(DataUtils::StringToQString(strIconDeleteFilePath));
    buttonDelete = new QPushButton;
    buttonDelete->setIcon(pixmap);
    buttonDelete->setIconSize(pixmap.rect().size());
    buttonDelete->setFixedSize(pixmap.rect().size());
    m_gridLayout->addWidget(buttonDelete, m_itemMap.size(), 0);

    QLineEdit* lineEdit = new QLineEdit;
    m_gridLayout->addWidget(lineEdit, m_itemMap.size(), 1);

    m_itemMap[buttonDelete] = lineEdit;
}

void NodeWidget::Init()
{
    QWidget* container = new QWidget;

    QVBoxLayout* vbLayout = new QVBoxLayout(container);
    
    m_gridLayout = new QGridLayout; 
    vbLayout->addLayout(m_gridLayout);

    QPixmap pixmap(DataUtils::StringToQString(strIconAddFilePath));
    m_buttonAdd = new QPushButton;
    m_buttonAdd->setIcon(pixmap);
    m_buttonAdd->setIconSize(pixmap.rect().size());
    m_buttonAdd->setFixedSize(pixmap.rect().size());
    QObject::connect(m_buttonAdd, SIGNAL(clicked()), this, SLOT(OnAdd()));
    vbLayout->addWidget(m_buttonAdd);

    setWidget(container);
}