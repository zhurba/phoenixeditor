#include <QtWidgets/QGridLayout>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMenu>
#include <QtGui/QContextMenuEvent>

#include "CentralWidgetDictionary.h"
#include "Strings.h"
#include "DataUtils.h"
#include "DataManager.h"
#include "CatalogDataImpl.h"

CentralWidgetDictionary::CentralWidgetDictionary(QWidget* parent)
    : CentralWidgetBase(parent)
    , m_table(nullptr)
    , m_contextMenu(nullptr)
{
    Init();
}

CentralWidgetDictionary::~CentralWidgetDictionary()
{
}

ECatalogDataType CentralWidgetDictionary::GetType() const
{
    return ECatalogDataType::Dictionary;
}

void CentralWidgetDictionary::contextMenuEvent(QContextMenuEvent* event)
{
    m_contextMenu->exec(event->globalPos());
}

void CentralWidgetDictionary::OnItemChanged(QTableWidgetItem* item)
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DictionaryDataImpl* pData = dynamic_cast<DictionaryDataImpl*>(activeNode->GetData().GetImpl());

        const int rowCurrent = item->row();
        const int columnCurrent = item->column();

        if (pData && rowCurrent < pData->m_dictionary.size())
        {
            QTableWidgetItem* wordItem = m_table->item(rowCurrent, 0);
            QTableWidgetItem* translationItem = m_table->item(rowCurrent, 1);
            if (wordItem)
            {
                pData->m_dictionary[rowCurrent].first = DataUtils::QStringToWString(wordItem->text());
            }
            if (translationItem)
            {
                pData->m_dictionary[rowCurrent].second = DataUtils::QStringToWString(translationItem->text());
            }
        }
    }
}

void CentralWidgetDictionary::OnItemAdd()
{
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DictionaryDataImpl* pData = dynamic_cast<DictionaryDataImpl*>(activeNode->GetData().GetImpl());
        if (pData)
        {
            pData->m_dictionary.emplace_back();
            m_table->setRowCount(pData->m_dictionary.size());
        }
    }
}

void CentralWidgetDictionary::OnItemDelete()
{

}

void CentralWidgetDictionary::OnItemMoveUp(bool checked)
{

}

void CentralWidgetDictionary::OnItemMoveDown(bool checked)
{

}

void CentralWidgetDictionary::Init()
{
    m_table = new QTableWidget;
    m_table->setColumnCount(2);
    m_table->setHorizontalHeaderLabels(QStringList() << "Word" << "Translation");
    m_table->verticalHeader()->hide();
    auto connectionItemChanged = QObject::connect(m_table, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(OnItemChanged(QTableWidgetItem*)));
    m_baseLayout->addWidget(m_table, 1, 0);

    int rowCount = 0;
    std::shared_ptr<CatalogNode> activeNode = DataManager::Instance()->GetActiveNode();
    if (activeNode)
    {
        DictionaryDataImpl* pData = dynamic_cast<DictionaryDataImpl*>(activeNode->GetData().GetImpl());
        if (pData)
        {
            rowCount = pData->m_dictionary.size();
            m_table->setRowCount(rowCount);
            for (size_t i = 0; i < pData->m_dictionary.size(); ++i)
            {
                QTableWidgetItem* wordItem = m_table->item(i, 0);
                if (!wordItem)
                {
                    wordItem = new QTableWidgetItem;
                    m_table->setItem(i, 0, wordItem);
                }
                wordItem->setText(DataUtils::WStringToQString(pData->m_dictionary[i].first));
                QTableWidgetItem* translationItem = m_table->item(i, 1);
                if (!translationItem)
                {
                    translationItem = new QTableWidgetItem;
                    m_table->setItem(i, 1, translationItem);
                }
                translationItem->setText(DataUtils::WStringToQString(pData->m_dictionary[i].second));
            }
        }
    }

    InitContextMenu();
}

// todo: context menu for table:

void CentralWidgetDictionary::InitContextMenu()
{
    QAction* actionAddAbove = new QAction(DataUtils::StringToQString(strAddAboveContextItem), this);
    auto connectionItemAddAbove = QObject::connect(actionAddAbove, SIGNAL(triggered(bool)), this, SLOT(OnItemAdd()));

    QAction* actionAddBelow = new QAction(DataUtils::StringToQString(strAddBelowContextItem), this);
    auto connectionItemAddBelow = QObject::connect(actionAddBelow, SIGNAL(triggered(bool)), this, SLOT(OnItemAdd()));

    QAction* actionDelete = new QAction(DataUtils::StringToQString(strDeleteContextItem), this);
    auto connectionItemDelete = QObject::connect(actionDelete, SIGNAL(triggered(bool)), this, SLOT(OnItemDelete()));

    QAction* actionMoveUp = new QAction("Move Up", this);
    auto connectionItemMoveUp = QObject::connect(actionMoveUp, SIGNAL(triggered(bool)), this, SLOT(OnItemMoveUp(bool)));

    QAction* actionMoveDown = new QAction("Move Down", this);
    auto connectionItemMoveDown = QObject::connect(actionMoveDown, SIGNAL(triggered(bool)), this, SLOT(OnItemMoveDown(bool)));

    m_contextMenu = new QMenu(m_table);
    m_contextMenu->addAction(actionAddAbove);
    m_contextMenu->addAction(actionAddBelow);
    m_contextMenu->addAction(actionDelete);
    m_contextMenu->addAction(actionMoveUp);
    m_contextMenu->addAction(actionMoveDown);
}