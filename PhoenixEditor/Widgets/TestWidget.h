#ifndef WIDGET_H
#define WIDGET_H

#include <QtWidgets/QWidget>

// This is a test Qt class
class TestWidget : public QWidget
{
    Q_OBJECT

public:
    TestWidget(QWidget* parent = 0);
    virtual ~TestWidget();

    void paintEvent(QPaintEvent* event);

private:
    void Init();
};

#endif
