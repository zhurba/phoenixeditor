#ifndef CENTRAL_WIDGET_DICTIONARY_H
#define CENTRAL_WIDGET_DICTIONARY_H

#include "CentralWidgetBase.h"

class QTableWidget;
class QTableWidgetItem;
class QMenu;

class CentralWidgetDictionary : public CentralWidgetBase
{
    Q_OBJECT

public:
    CentralWidgetDictionary(QWidget* parent = nullptr);
    virtual ~CentralWidgetDictionary();

public:
    ECatalogDataType GetType() const override;
    void contextMenuEvent(QContextMenuEvent* event) override;

private Q_SLOTS:
    void OnItemChanged(QTableWidgetItem* item);
    void OnItemAdd();
    void OnItemDelete();
    void OnItemMoveUp(bool checked);
    void OnItemMoveDown(bool checked);

private:
    void Init();
    void InitContextMenu();

private:
    QTableWidget* m_table;
    QMenu* m_contextMenu;
};

#endif
