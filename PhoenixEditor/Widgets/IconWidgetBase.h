#ifndef ICON_WIDGET_H
#define ICON_WIDGET_H

#include <QtWidgets/QWidget>

#include "DataTypes.h"

class QLineEdit;

class IconWidgetBase : public QWidget
{
    Q_OBJECT

public:
    IconWidgetBase(QWidget* parent, std::shared_ptr<CatalogNode> spData);
    virtual ~IconWidgetBase();

public:
    void SetProxyWidget(QGraphicsProxyWidget* gpWidget);

protected:
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseMoveEvent(QMouseEvent* event) override;

    void OnDataChanged(const CatalogData& data);

protected Q_SLOTS:
    void OnTextChanged(const QString& text);

private:
    void Init();
    void Update();

private:
    std::shared_ptr<CatalogNode> m_spData;
    std::shared_ptr<CatalogNode::FuncType> m_spReceiver;
    QGraphicsProxyWidget* m_gpWidget;
    QLineEdit* m_leTitle;
    int m_posX;
    int m_posY;
};

#endif
