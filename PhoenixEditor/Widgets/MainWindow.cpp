#include <QtWidgets/QMenuBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QFileDialog>

#include "MainWindow.h"
#include "CentralWidgetBase.h"
#include "CatalogWidget.h"
#include "NodeWidget.h"
#include "WidgetFactory.h"
#include "DataUtils.h"
#include "Constants.h"

#include "Strings.h"
#include "ParserXML.h"
#include "DataManager.h"
#include "FileManager.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , m_centralWidgets()
    , m_currentCentralWidget(nullptr)
    , m_catalogWidget(nullptr)
    , m_nodeWidget(nullptr)
    , m_menuRecentFiles(nullptr)
    , m_spReceiver(nullptr)
{
    Init();
}

MainWindow::~MainWindow()
{
    for (auto widget : m_centralWidgets)
    {
        delete widget.second;
    }
    m_centralWidgets.clear();
    m_currentCentralWidget = nullptr;
    m_catalogWidget = nullptr;
    m_nodeWidget = nullptr;

    DataManager::Instance()->GetActiveNodeChanged()->UnregisterReceiver(m_spReceiver);
}

void MainWindow::OnActiveNodeChanged(const SenderParamsBase* params)
{
    if (auto cnParams = dynamic_cast<const SenderParams<std::shared_ptr<CatalogNode>>*>(params))
    {
        if (cnParams->m_arg1)
        {
            const CatalogData& data = cnParams->m_arg1->GetData();

            if (!m_currentCentralWidget || m_currentCentralWidget->GetType() != data.GetType())
            {
                UpdateCentralWidget(data.GetType());
            }

            std::wstring title = data.GetTitle();
            if (m_currentCentralWidget)
            {
                m_currentCentralWidget->SetTitle(title);
            }
        }
        else
        {
            UpdateCentralWidget(ECatalogDataType::None);
        }
    }
}

void MainWindow::OnActionNew()
{
    DataManager::Instance()->SetFileName(""); // strCatalogFilePath
    DataManager::Instance()->ClearData();
    m_catalogWidget->UpdateCatalog();
}

void MainWindow::OnActionLoad()
{
    QAction* action = dynamic_cast<QAction*>(QObject::sender());
    if (action)
    {
        std::string fileName = DataUtils::QStringToString(action->text());
        LoadFile(fileName);
    }
}

void MainWindow::OnActionLoadAs()
{
    QString qFileName = QFileDialog::getOpenFileName(this);
    if (!qFileName.isEmpty())
    {
        std::string fileName = DataUtils::QStringToString(qFileName);
        LoadFile(fileName);
    }
}

void MainWindow::OnActionSave()
{
    const std::string& fileName = DataManager::Instance()->GetFileName();
    if (!fileName.empty())
    {
        SaveFile(fileName);
    }
    else
    {
        OnActionSaveAs();
    }
}

void MainWindow::OnActionSaveAs()
{
    QString qFileName = QFileDialog::getSaveFileName(this);
    if (!qFileName.isEmpty())
    {
        std::string fileName = DataUtils::QStringToString(qFileName);
        if (fileName != DataManager::Instance()->GetFileName())
        {
            DataManager::Instance()->SetFileName(fileName); // strCatalogFilePath
            FileManager::Instance()->AddRecentFile(fileName);
            UpdateMenu();
        }
        SaveFile(fileName);
    }
}

void MainWindow::LoadFile(const std::string& fileName)
{
    if (fileName.empty())
    {
        return;
    }

    DataManager::Instance()->SetFileName(fileName); // strCatalogFilePath
    FileManager::Instance()->AddRecentFile(fileName);

    DataManager::Instance()->ClearData();
    ParserXML::Instance()->DeSerialize(DataManager::Instance()->GetFileName());
    m_catalogWidget->UpdateCatalog();
    UpdateMenu();
}

void MainWindow::SaveFile(const std::string& fileName)
{
    if (fileName.empty())
    {
        return;
    }

    std::shared_ptr<CatalogNode> spRootNode = DataManager::Instance()->GetRootNode();
    ParserXML::Instance()->Serialize(fileName, spRootNode);
}

void MainWindow::Init()
{
    setMinimumWidth(MIN_WINDOW_WIDTH);
    setMinimumHeight(MIN_WINDOW_HEIGHT);

    QAction* actionNew = new QAction(DataUtils::StringToQString("New"), this);
    auto connectionNew = QObject::connect(actionNew, SIGNAL(triggered(bool)), this, SLOT(OnActionNew()));

    QAction* actionLoadAs = new QAction(DataUtils::StringToQString("Load..."), this);
    auto connectionLoadAs = QObject::connect(actionLoadAs, SIGNAL(triggered(bool)), this, SLOT(OnActionLoadAs()));

    QAction* actionSave = new QAction(DataUtils::StringToQString("Save"), this);
    auto connectionSave = QObject::connect(actionSave, SIGNAL(triggered(bool)), this, SLOT(OnActionSave()));

    QAction* actionSaveAs = new QAction(DataUtils::StringToQString("Save As..."), this);
    auto connectionSaveAs = QObject::connect(actionSaveAs, SIGNAL(triggered(bool)), this, SLOT(OnActionSaveAs()));

    QMenuBar* menuBar = new QMenuBar(this);
    QMenu* menuFile = menuBar->addMenu(DataUtils::StringToQString("File"));
    menuFile->addAction(actionNew);
    menuFile->addAction(actionLoadAs);
    menuFile->addAction(actionSave);
    menuFile->addAction(actionSaveAs);
    m_menuRecentFiles = menuFile->addMenu("Recent Files");
    UpdateMenu();

    setMenuBar(menuBar);

    QToolBar* toolBar = new QToolBar(this);
    toolBar->addAction("Load", this, SLOT(OnActionLoadAs()));
    toolBar->addAction("Save", this, SLOT(OnActionSave()));
    addToolBar(Qt::TopToolBarArea, toolBar);

    UpdateCentralWidget(ECatalogDataType::None);

    m_catalogWidget = new CatalogWidget;
    QDockWidget* catalogDockWidget = new QDockWidget(QObject::tr("Catalog"), this);
    catalogDockWidget->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    catalogDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    catalogDockWidget->setWidget(m_catalogWidget);
    addDockWidget(Qt::LeftDockWidgetArea, catalogDockWidget);

    m_nodeWidget = new NodeWidget(this);
    m_nodeWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, m_nodeWidget);

    m_spReceiver = std::make_shared<Receiver<MainWindow>>(this, &MainWindow::OnActiveNodeChanged);
    DataManager::Instance()->GetActiveNodeChanged()->RegisterReceiver(m_spReceiver);
}

void MainWindow::UpdateMenu()
{
    auto& recentFiles = FileManager::Instance()->GetRecentFiles();
    m_menuRecentFiles->clear();
    for (auto& recentFileName : recentFiles)
    {
        m_menuRecentFiles->addAction(DataUtils::StringToQString(recentFileName), this, &MainWindow::OnActionLoad);
    }
}

void MainWindow::UpdateCentralWidget(ECatalogDataType dataType)
{
    if (!m_centralWidgets[dataType])
    {
        m_centralWidgets[dataType] = WidgetFactory::Instance()->CreateCentralWidget(this, dataType);
    }

    if (m_centralWidgets[dataType] != m_currentCentralWidget)
    {
        takeCentralWidget();
        m_currentCentralWidget = m_centralWidgets[dataType];
        setCentralWidget(m_currentCentralWidget);
    }
}
