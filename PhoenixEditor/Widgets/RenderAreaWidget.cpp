#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsProxyWidget>
#include <QtWidgets/QVBoxLayout>

#include "RenderAreaWidget.h"
#include "WidgetFactory.h"

RenderAreaWidget::RenderAreaWidget(QWidget* parent)
    : QWidget(parent)
    , m_gView(nullptr)
    , m_gScene(nullptr)
    , m_iconWidgets()
{
    Init();
}

RenderAreaWidget::~RenderAreaWidget()
{
}

void RenderAreaWidget::Init()
{
    m_gScene = new QGraphicsScene;
    m_gView = new QGraphicsView(m_gScene);

    QPen pen(QBrush(Qt::red), 5, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
    QGraphicsLineItem* lineItem = m_gScene->addLine(0, m_gView->height(), m_gView->width(), 0, pen);

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(m_gView); // todo: remove reparenting?
}

void RenderAreaWidget::AddIconWidget(std::shared_ptr<CatalogNode> spNode)
{
    if (!m_gScene)
        return;

    IconWidgetBase* iconWidget = WidgetFactory::Instance()->CreateIconWidget(nullptr, spNode);
    QGraphicsProxyWidget* gpWidget = m_gScene->addWidget(iconWidget);
    gpWidget->setVisible(true);
    gpWidget->setPos(25, 25);
    gpWidget->setZValue(-1);
    iconWidget->SetProxyWidget(gpWidget);
    m_iconWidgets.push_back(std::shared_ptr<IconWidgetBase>(iconWidget));
}