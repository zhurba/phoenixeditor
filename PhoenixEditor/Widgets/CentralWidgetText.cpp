#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>

#include "CentralWidgetText.h"
#include "DataManager.h"

CentralWidgetText::CentralWidgetText(QWidget* parent)
    : CentralWidgetBase(parent)
{
    Init();
}

CentralWidgetText::~CentralWidgetText()
{
}

ECatalogDataType CentralWidgetText::GetType() const
{
    return ECatalogDataType::Text;
}

void CentralWidgetText::Init()
{
    QLineEdit* textEdit1 = new QLineEdit("Text", this);
    m_baseLayout->addWidget(textEdit1, 0, 0);

    QTextEdit* textEdit2 = new QTextEdit("Description", this);
    m_baseLayout->addWidget(textEdit2, 1, 0);

    // Sample code:
    /*std::shared_ptr<CatalogNode> node = DataManager::Instance()->FindNode(L"�������");
    if (node)
    {
        m_renderAreaWidget->AddIconWidget(node);
    }*/
}
