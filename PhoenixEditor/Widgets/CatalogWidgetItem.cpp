#include <functional>

#include "CatalogWidgetItem.h"
#include "DataManager.h"
#include "DataUtils.h"

CatalogWidgetItem::CatalogWidgetItem(QTreeWidgetItem* parent, std::shared_ptr<CatalogNode> spData)
    : QTreeWidgetItem(parent)
    , m_spData(spData)
    , m_spReceiver(nullptr)
{
    m_spReceiver = std::make_shared<CatalogNode::FuncType>(std::bind(&CatalogWidgetItem::OnDataChanged, this, std::placeholders::_1));
    m_spData->RegisterReceiver(m_spReceiver);

    Init();
}

CatalogWidgetItem::~CatalogWidgetItem()
{
}

void CatalogWidgetItem::Activate() const
{
    DataManager::Instance()->SetActiveNode(m_spData);
}

void CatalogWidgetItem::SetTitle(int column)
{
    const QString qData = text(column);
    std::wstring title = DataUtils::QStringToWString(qData);

    CatalogData newData(m_spData->GetData());
    newData.SetTitle(title);

    m_spData->UnregisterReceiver(m_spReceiver);
    m_spData->SetData(newData);
    m_spData->RegisterReceiver(m_spReceiver);
}

void CatalogWidgetItem::OnDataChanged(const CatalogData& data)
{
    Init();
}

void CatalogWidgetItem::Init()
{
    const std::wstring& title = m_spData->GetData().GetTitle();
    QString qData = DataUtils::WStringToQString(title);

    bool blocked = false;
    if (treeWidget())
        blocked = treeWidget()->blockSignals(true);
    setText(0, qData);
    if (treeWidget())
        treeWidget()->blockSignals(blocked);
}
