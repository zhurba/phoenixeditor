#ifndef CATALOG_WIDGET_H
#define CATALOG_WIDGET_H

#include <QtWidgets/QWidget>

#include "Notifier.h"
#include "DataTypes.h"

class QPushButton;
class QTreeWidget;
class QTreeWidgetItem;
class CatalogWidgetItem;
class QMenu;

class CatalogWidget : public QWidget
{
    Q_OBJECT

public:
    CatalogWidget(QWidget* parent = 0);
    virtual ~CatalogWidget();

    void UpdateCatalog();

    void OnActiveNodeChanged(const SenderParamsBase* params);

    void contextMenuEvent(QContextMenuEvent* event) override;

public Q_SLOTS:
    void OnItemPressed(QTreeWidgetItem* item, int column);
    void OnItemChanged(QTreeWidgetItem* item, int column);
    void OnItemAdd();
    void OnItemDelete();
    void OnItemMoveUp(bool checked);
    void OnItemMoveDown(bool checked);

private:
    void Init();
    void InitContextMenu();
    CatalogWidgetItem* CreateItem(std::shared_ptr<CatalogNode> spNode, CatalogWidgetItem* parent);

private:
    QPushButton* m_pbAddItem;
    QPushButton* m_pbDeleteItem;
    QTreeWidget* m_tree;
    QMenu* m_contextMenu;
    std::shared_ptr<Receiver<CatalogWidget>> m_spReceiver;
};

#endif
