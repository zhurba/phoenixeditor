#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtWidgets/QMainWindow>

#include "Notifier.h"
#include "EnumTypes.h"

class CentralWidgetBase;
class CatalogWidget;
class NodeWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    virtual ~MainWindow();

    void OnActiveNodeChanged(const SenderParamsBase* params);

public Q_SLOTS:
    void OnActionNew();
    void OnActionLoad();
    void OnActionLoadAs();
    void OnActionSave();
    void OnActionSaveAs();

private:
    void LoadFile(const std::string& fileName);
    void SaveFile(const std::string& fileName);
    void Init();
    void UpdateMenu();
    void UpdateCentralWidget(ECatalogDataType dataType);

private:
    std::map<ECatalogDataType, CentralWidgetBase*> m_centralWidgets;
    CentralWidgetBase* m_currentCentralWidget;
    CatalogWidget* m_catalogWidget;
    NodeWidget* m_nodeWidget;
    QMenu* m_menuRecentFiles;
    std::shared_ptr<Receiver<MainWindow>> m_spReceiver;
};

#endif
