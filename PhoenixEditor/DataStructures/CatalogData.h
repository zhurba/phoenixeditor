#ifndef CATALOG_DATA_H
#define CATALOG_DATA_H

#include <string>

#include "EnumTypes.h"

class CatalogDataImpl;

class CatalogData
{
public:
    CatalogData();
    CatalogData(const std::wstring& name, const std::wstring& title);
    CatalogData(const CatalogData& other);
    virtual ~CatalogData();

    const CatalogData& operator=(const CatalogData& other);

    std::wstring GetName() const { return m_name; }
    void SetName(const std::wstring& name) { m_name = name; }

    std::wstring GetTitle() const { return m_title; }
    void SetTitle(const std::wstring& title) { m_title = title; }

    std::wstring GetContent() const { return m_content; }
    void SetContent(const std::wstring& content) { m_content = content; }

    ECatalogDataType GetType() const { return m_type; }
    void SetType(const ECatalogDataType type) { m_type = type; }
    bool HasType() const { return m_type >= ECatalogDataType::Graph && m_type < ECatalogDataType::Count; }

    CatalogDataImpl* GetImpl() const { return m_pImpl; }

    static ECatalogDataType WStringToCatalogDataType(const std::wstring& str);
    static std::wstring CatalogDataTypeToWString(const ECatalogDataType type);

private:
    std::wstring m_name;
    std::wstring m_title;
    std::wstring m_content;
    ECatalogDataType m_type;
    CatalogDataImpl* m_pImpl;
};

#endif