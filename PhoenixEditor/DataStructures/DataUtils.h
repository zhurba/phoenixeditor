#ifndef DATA_UTILS_H
#define DATA_UTILS_H

#include <string>
#include <QtCore/QString>

class DataUtils
{
public:
    DataUtils() {}
    ~DataUtils() {}

public:
    static std::wstring QStringToWString(const QString& qString);
    static std::string QStringToString(const QString& qString);
    static QString WStringToQString(const std::wstring& wString);
    static QString StringToQString(const std::string& string);
    static std::wstring StringToWString(const std::string& string);
    static std::string WStringToString(const std::wstring& wString);
};

#endif
