#ifndef PARSER_XML_H
#define PARSER_XML_H

#include <string>
#include <memory>
#include <map>

#include "DataTypes.h"

using CatalogNode = TreeNode<CatalogData>;

class Snippet
{
public:
    enum class ESnippetType
    {
        Catalog = 0,
        Dictionary,
        Count,
        Invalid
    };

public:
    Snippet(const std::wstring& wStr, const size_t startPos = -1, const size_t endPos = -1)
        : m_wStr(wStr)
        , m_startPos(startPos)
        , m_endPos(endPos)
    {}
    ~Snippet() {}

    const std::wstring& GetString() const { return m_wStr; }
    size_t GetStartPos() const { return m_startPos; }
    size_t GetEndPos() const { return m_endPos; }

    void SetStartPos(size_t startPos) { m_startPos = startPos; }
    void SetEndPos(size_t endPos) { m_endPos = endPos; }

    bool IsValid() const
    {
        return m_startPos >= 0
            && m_endPos >= 0
            && m_startPos <= m_endPos
            && m_startPos <= m_wStr.size()
            && m_endPos <= m_wStr.size();
    }

private:
    const std::wstring& m_wStr;
    size_t m_startPos;
    size_t m_endPos;
};

class Tag
{
public:
    enum class ETagType
    {
        TagOpen = 0,
        TagClose,
        TagSingle,
        TagCount,
        TagInvalid
    };

public:
    Tag(Snippet& snippet)
    : m_data()
    , m_type(ETagType::TagInvalid)
    {
        Parse(snippet);
    }
    ~Tag() {}

    CatalogData& GetDataRef() { return m_data; }
    const CatalogData& GetData() const { return m_data; }
    //void SetData(const CatalogData& data) { m_data = data; }

    ETagType GetType() const { return m_type; }
    void SetType(const ETagType type) { m_type = type; }

    bool IsOpen() const { return m_type == ETagType::TagOpen; }
    bool IsClose() const { return m_type == ETagType::TagClose; }
    bool IsSingle() const { return m_type == ETagType::TagSingle; }
    bool IsValid() const { return IsOpen() || IsClose() || IsSingle(); }

private:
    void ParseAttribute(const std::wstring& attrString);
    void ParseAttributes(const std::wstring& tagString);
    void Parse(Snippet& remain);

private:
    CatalogData m_data;
    ETagType m_type;
    std::map<std::wstring, std::wstring> m_attributes;
};

class ParserXML
{
public:
    ~ParserXML();
    static ParserXML* Instance();

public:
    bool DeSerialize(const std::string& fileName);
    void Serialize(const std::string& fileName, const std::shared_ptr<CatalogNode> treeNode);

private:
    ParserXML();
    ParserXML(const ParserXML&) = delete;
    ParserXML& operator=(const ParserXML&) = delete;

    bool DeSerializeSnippet(Snippet& snippet, std::shared_ptr<CatalogNode> parent, Snippet::ESnippetType snippetType) const;
    std::wstring SerializeSnippet(std::shared_ptr<CatalogNode> node, int level = 0) const;

    void DeSerializeNodeFile(std::shared_ptr<CatalogNode> node) const;
    void SerializeNodeFile(std::shared_ptr<CatalogNode> node) const;

    std::wstring SerializeAttributes(std::shared_ptr<CatalogNode> node) const;

    //std::shared_ptr<CatalogNode> ConvertToNode(const Tag& tag, std::shared_ptr<CatalogNode> parent) const;
    void TagToDictionary(const Tag& tag, std::shared_ptr<CatalogNode> node) const;

    std::wstring GetNodeFileName(std::shared_ptr<CatalogNode> node, const std::wstring& folderName) const;

private:
    std::wstring m_rootFileName;
};

#endif
