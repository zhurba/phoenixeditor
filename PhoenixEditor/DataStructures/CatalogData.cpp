#include "CatalogData.h"
#include "CatalogDataImpl.h"
#include "Strings.h"

CatalogData::CatalogData()
    : m_name()
    , m_title()
    , m_content()
    , m_type(ECatalogDataType::None)
    , m_pImpl(nullptr)
{
    if (m_type == ECatalogDataType::Dictionary)
    {
        m_pImpl = new DictionaryDataImpl();
    }
}

CatalogData::CatalogData(const std::wstring& name, const std::wstring& title)
    : m_name(name)
    , m_title(title)
    , m_content()
    , m_type(ECatalogDataType::None)
    , m_pImpl(nullptr)
{
    if (m_type == ECatalogDataType::Dictionary)
    {
        m_pImpl = new DictionaryDataImpl();
    }
}

CatalogData::CatalogData(const CatalogData& other)
    : m_name(other.m_name)
    , m_title(other.m_title)
    , m_content(other.m_content)
    , m_type(other.m_type)
    , m_pImpl(nullptr)
{
    if (m_type == ECatalogDataType::Dictionary)
    {
        m_pImpl = new DictionaryDataImpl();
    }
}

CatalogData::~CatalogData()
{
    if (m_type == ECatalogDataType::Dictionary)
    {
        delete m_pImpl;
    }
}

const CatalogData& CatalogData::operator=(const CatalogData& other)
{
    m_name = other.m_name;
    m_title = other.m_title;
    m_content = other.m_content;
    return *this;
}

ECatalogDataType CatalogData::WStringToCatalogDataType(const std::wstring& str)
{
    if (str == strCatalogDataTypeTree)
    {
        return ECatalogDataType::Tree;
    }
    else if (str == strCatalogDataTypeMap)
    {
        return ECatalogDataType::Graph;
    }
    else if (str == strCatalogDataTypeText)
    {
        return ECatalogDataType::Text;
    }
    else if (str == strCatalogDataTypeDictionary)
    {
        return ECatalogDataType::Dictionary;
    }

    return ECatalogDataType::None;
}

std::wstring CatalogData::CatalogDataTypeToWString(const ECatalogDataType type)
{
    switch (type)
    {
    case ECatalogDataType::Tree:
        return strCatalogDataTypeTree;
    case ECatalogDataType::Graph:
        return strCatalogDataTypeMap;
    case ECatalogDataType::Text:
        return strCatalogDataTypeText;
    case ECatalogDataType::Dictionary:
        return strCatalogDataTypeDictionary;
    default:
        return strEmpty;
    }
}
