#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <list>
#include <memory>
#include <functional>

/// ------------------------- Receivers ------------------------- ///

class SenderParamsBase
{
public:
    SenderParamsBase() {}
    virtual ~SenderParamsBase() {}
};

/*template <typename Arg1>
class SenderParams : public SenderParamsBase
{
public:
    SenderParams(Arg1 arg1)
        : SenderParamsBase()
        , m_arg1(arg1)
    {}
    virtual ~SenderParams() {}
    Arg1 m_arg1;
};*/

template <typename... Args>
class SenderParams : public SenderParamsBase
{
public:
    SenderParams()
        : SenderParamsBase()
    {}
    virtual ~SenderParams() {}
};

template <typename Arg1, typename... Args>
class SenderParams<Arg1, Args...> : public SenderParamsBase
{
public:
    SenderParams(Arg1 arg1)
        : SenderParamsBase()
        , m_arg1(arg1)
    {}
    virtual ~SenderParams() {}
    Arg1 m_arg1;
};

template <typename Arg1, typename Arg2, typename... Args>
class SenderParams<Arg1, Arg2, Args...> : public SenderParamsBase
{
public:
    SenderParams(Arg1 arg1, Arg2 arg2)
        : SenderParamsBase()
        , m_arg1(arg1)
        , m_arg2(arg2)
    {}
    virtual ~SenderParams() {}
    Arg1 m_arg1;
    Arg2 m_arg2;
};

class ReceiverBase
{
public:
    virtual ~ReceiverBase() {}

    virtual void operator()(const SenderParamsBase*) = 0;
};

template <typename ObjectType>
class Receiver : public ReceiverBase
{
public:
    Receiver(ObjectType* receiverObject, void(ObjectType::* receiverFunction)(const SenderParamsBase*))
    {
        m_slot = std::bind(receiverFunction, receiverObject, std::placeholders::_1);
    }

    virtual ~Receiver() override {}

    void operator()(const SenderParamsBase* params) override
    {
        m_slot(params);
    }

private:
    std::function<void(const SenderParamsBase*)> m_slot;
};

/// ------------------------- Senders ------------------------- ///

class Sender
{
public:
    void RegisterReceiver(std::shared_ptr<ReceiverBase> spReceiver)
    {
        std::weak_ptr<ReceiverBase> wpReceiver = spReceiver;
        m_wpReceivers.push_back(wpReceiver);
    }

    void UnregisterReceiver(std::shared_ptr<ReceiverBase> spReceiver)
    {
        for (auto iter = m_wpReceivers.begin(); iter != m_wpReceivers.end(); ++iter)
        {
            if (auto spIterReceiver = iter->lock())
            {
                if (spIterReceiver.get() == spReceiver.get())
                {
                    m_wpReceivers.erase(iter);
                    break;
                }
            }
        }
    }

    void Notify(const SenderParamsBase* params)
    {
        auto iter = m_wpReceivers.begin();
        while(iter != m_wpReceivers.end())
        {
            if (auto wpReceiver = iter->lock())
            {
                (*wpReceiver)(params);
                ++iter;
            }
            else
            {
                auto iterToErase = iter++;
                m_wpReceivers.erase(iterToErase);
            }
        }
    }

private:
    std::list<std::weak_ptr<ReceiverBase>> m_wpReceivers;
};

/// ------------------------- Notifiers ------------------------- ///

class ListenerObject
{
public:
    virtual ~ListenerObject() {}
};

// For now, only for 1 parameter:
template <typename ...Args>
class Notifier
{
public:
    template <typename ListenerType>
    void AddListener(ListenerType* listenerObj, void (ListenerType::* listenerFunc)(Args... args))
    {
        // constexpr std::size_t size = sizeof...(Args); // get count of parameters
        std::function<void(Args...)> func = std::bind(listenerFunc, listenerObj, std::placeholders::_1);
        m_listeners.push_back(std::make_pair(listenerObj, std::bind(listenerFunc, listenerObj, std::placeholders::_1)));
    }

    template <typename ListenerType>
    void RemoveListener(ListenerType* listenerObj, void (ListenerType::* listenerFunc)(Args... args))
    {
        for (auto iter = m_listeners.begin(); iter != m_listeners.end(); ++iter)
        {
            if (iter->first == listenerObj)
            {
                m_listeners.erase(iter);
                break;
            }
        }
    }

    void Notify(Args... args)
    {
        typename std::list<std::pair<ListenerObject*, std::function<void(Args...)>>>::iterator iter = m_listeners.begin();
        while (iter != m_listeners.end())
        {
            std::function<void(Args...)> func = (*iter).second;
            func(args...);
            ++iter;
        }
    }

private:
    std::list<std::pair<ListenerObject*, std::function<void(Args...)>>> m_listeners;
};

template <>
class Notifier<>
{
    template <typename ListenerType>
    void AddListener(ListenerType* listenerObj, void (ListenerType::* listenerFunc)())
    {
        std::function<void()> func = std::bind(listenerFunc, listenerObj);
        m_listeners.push_back(std::make_pair(listenerObj, std::bind(listenerFunc, listenerObj)));
    }

private:
    std::list<std::pair<ListenerObject*, std::function<void()>>> m_listeners;
};

/// ------------------------- Examples ------------------------- ///

/*
#include <iostream>

class ParamsA : public SenderParamsBase
{};
class ClassA : public Object
{
public:
    void foo(const SenderParamsBase* params)
    {
        std::wcout << L"ClassA::foo\n";
    }

    void bar(int a)
    {
        std::wcout << L"ClassA::bar\n";
    }
};
class ClassB : public Object
{
public:
    void func(const SenderParamsBase* params)
    {
        std::wcout << L"ClassB::func\n";
    }

    void jazz(int a)
    {
        std::wcout << L"ClassB::jazz\n";
    }
};
void main()
{
    Sender sender;
    ClassA* objA = new ClassA;
    ClassB* objB = new ClassB;
    std::shared_ptr<ReceiverBase> receiver0A = std::make_shared<Receiver<ClassA>>(objA, &ClassA::foo);
    std::shared_ptr<ReceiverBase> receiver0B = std::make_shared<Receiver<ClassB>>(objB, &ClassB::func);
    sender.RegisterReceiver(receiver0A);
    sender.RegisterReceiver(receiver0B);
    ParamsA paramsA;
    sender.Notify(&paramsA);
    sender.UnregisterReceiver(receiver0A);
    sender.UnregisterReceiver(receiver0B);

    Notifier<int> notifier;
    notifier.AddListener(objA, &ClassA::bar);
    notifier.AddListener(objB, &ClassB::jazz);
    int intVal = 1;
    notifier.Notify(intVal);
    notifier.RemoveListener(objA, &ClassA::bar);
    notifier.RemoveListener(objB, &ClassB::jazz);
}
*/


#endif
