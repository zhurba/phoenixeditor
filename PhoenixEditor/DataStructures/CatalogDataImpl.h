#ifndef CATALOG_DATA_IMPL_H
#define CATALOG_DATA_IMPL_H

#include <string>
#include <vector>

#include "EnumTypes.h"

class CatalogDataImpl
{
public:
    virtual ECatalogDataType GetType() const { return ECatalogDataType::None; }
};

class DictionaryDataImpl : public CatalogDataImpl
{
public:
    ECatalogDataType GetType() const override { return ECatalogDataType::Dictionary; }

public:
    std::vector<std::pair<std::wstring, std::wstring>> m_dictionary;
};

#endif