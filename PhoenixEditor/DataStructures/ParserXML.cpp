#include <iostream>
#include <fstream>
#include <sstream>
#include <codecvt>
#include <fcntl.h>
#include <io.h>
#include <filesystem>

#include "ParserXML.h"
#include "DataManager.h"
#include "Strings.h"
#include "CatalogDataImpl.h"
#include "DataUtils.h"

void Tag::ParseAttribute(const std::wstring& attrString)
{
    size_t posEqual = attrString.find(L"=");
    if (posEqual == std::string::npos)
    {
        return;
    }

    std::wstring attrName = attrString.substr(0, posEqual);
    std::wstring attrValue = attrString.substr(posEqual + 1, attrString.size() - posEqual - 1);
    if (!attrValue.empty() && attrValue[0] == L'\"')
    {
        attrValue = attrValue.substr(1, attrValue.size() - 2);
    }
    if (attrName == strAttrNameTitle)
    {
        GetDataRef().SetTitle(attrValue);
    }
    else if (attrName == strAttrNameType)
    {
        GetDataRef().SetType(CatalogData::WStringToCatalogDataType(attrValue));
    }
    else
    {
        // Attribute map (name-value):
        m_attributes[attrName] = attrValue;
    }
}

void Tag::ParseAttributes(const std::wstring& tagString)
{
    size_t size = tagString.size();
    size_t iAttr = 0;
    size_t startAttrPos = 0;
    size_t endAttrPos = 0;
    bool isInText = false; // to skip spaces inside quotes
    while (startAttrPos < size)
    {
        if (endAttrPos >= size || (tagString[endAttrPos] == L' ' && !isInText))
        {
            if (endAttrPos > startAttrPos)
            {
                std::wstring attrString = tagString.substr(startAttrPos, endAttrPos - startAttrPos);
                if (iAttr == 0)
                {
                    GetDataRef().SetName(attrString);
                }
                else
                {
                    ParseAttribute(attrString);
                }
                ++iAttr;
            }
            startAttrPos = endAttrPos + 1;
            endAttrPos = startAttrPos;
        }
        else
        {
            if (tagString[endAttrPos] == L'\"')
            {
                isInText = !isInText;
            }
            ++endAttrPos;
        }
    }
}

void Tag::Parse(Snippet& remain)
{
    size_t startTagPos = remain.GetString().find('<', remain.GetStartPos());
    if (startTagPos == std::wstring::npos || startTagPos >= remain.GetEndPos())
    {
        // Wrong string or end of buffer:
        //std::wcout << "Couldn`t find '<' symbol.\n";
        return;
    }

    size_t endTagPos = remain.GetString().find('>', startTagPos);
    if (startTagPos == std::wstring::npos)
    {
        std::wcout << "Couldn`t find '>' symbol.\n";
        return;
    }

    ++endTagPos; // End tag position points to the symbol after '>'

    remain.SetStartPos(endTagPos);

    // Remove brackets:
    ++startTagPos;
    --endTagPos;
    if (startTagPos >= endTagPos)
    {
        return;
    }

    // Define tag type:
    if (remain.GetString()[endTagPos - 1] == L'/')
    {
        SetType(Tag::ETagType::TagSingle);
        --endTagPos;
    }
    else if (remain.GetString()[startTagPos] == L'/')
    {
        SetType(Tag::ETagType::TagClose);
        ++startTagPos;
    }
    else
    {
        SetType(Tag::ETagType::TagOpen);
    }

    std::wstring tagString = remain.GetString().substr(startTagPos, endTagPos - startTagPos);

    // Parse attributes:
    ParseAttributes(tagString);

    return;
}

ParserXML::~ParserXML()
{
}

ParserXML* ParserXML::Instance()
{
    static ParserXML parser;
    return &parser;
}

bool ParserXML::DeSerialize(const std::string& fileName)
{
    bool result = false;

    m_rootFileName = DataUtils::StringToWString(fileName);
    std::wifstream ifs(fileName);

    // To treat file content as wide characters:
    ifs.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

    if (ifs.is_open())
    {
        std::wstringstream wss;
        wss << ifs.rdbuf();

        // To allow wstring to be printed by wcout:
        //_setmode(_fileno(stdout), _O_U16TEXT);
        //std::wcout << wss.str();

        std::wstring wStr = wss.str();
        Snippet snippet(wStr, 0, wStr.size());
        result = DeSerializeSnippet(snippet, nullptr, Snippet::ESnippetType::Catalog);

        ifs.close();
    }

    return result;
}

void ParserXML::Serialize(const std::string& fileName, const std::shared_ptr<CatalogNode> treeNode)
{
    m_rootFileName = DataUtils::StringToWString(fileName);
    std::wofstream ofs(fileName);

    // To treat file content as wide characters:
    ofs.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

    if (ofs.is_open())
    {
        std::wstring serializedTree;
        if (treeNode)
        {
            serializedTree = SerializeSnippet(treeNode);
        }

        // For debug:
        //std::wcout << serializedTree;

        ofs.write(serializedTree.c_str(), serializedTree.size());

        ofs.close();
    }
}

ParserXML::ParserXML()
{
}

bool ParserXML::DeSerializeSnippet(Snippet& snippet, std::shared_ptr<CatalogNode> parent, Snippet::ESnippetType snippetType) const
{
    while (snippet.GetStartPos() < snippet.GetEndPos())
    {
        size_t startTagPos = snippet.GetString().find('<', snippet.GetStartPos());
        if (startTagPos == std::wstring::npos || startTagPos >= snippet.GetEndPos())
        {
            // Wrong string or end of buffer:
            //std::wcout << "Couldn`t find '<' symbol.\n";
            return true;
        }
        // Store potential node content:
        std::wstring content = snippet.GetString().substr(snippet.GetStartPos(), startTagPos - snippet.GetStartPos()); // todo: think how to collect content for dictionary
        // Create tag which parses snippet and returns remain:
        Tag tag(snippet);
        if (!tag.IsValid())
        {
            std::wcout << "Invalid tag.\n";
            return false;
        }
        else if (tag.IsClose())
        {
            // Skip check since we don`t build hierarchy of nodes for dictionary type:
            if (snippetType == Snippet::ESnippetType::Dictionary)
            {
                continue;
            }

            // Close component:
            if (parent && parent->GetData().GetName() == tag.GetData().GetName())
            {
                return true;
            }
            else
            {
                std::wcout << "Close tag without corresponding open tag.\n";
                return false;
            }
        }
        else if (tag.IsOpen() || tag.IsSingle())
        {
            if (snippetType == Snippet::ESnippetType::Dictionary)
            {
                TagToDictionary(tag, parent);
            }
            else
            {
                std::shared_ptr<CatalogNode> treeNode = DataManager::Instance()->CreateNode(tag.GetData(), parent);
                DeSerializeNodeFile(treeNode);
                //std::shared_ptr<CatalogNode> treeNode = ConvertToNode(tag, parent);
                if (tag.IsOpen())
                {
                    // Append text between tags to tag`s content:
                    if (parent)
                    {
                        parent->GetDataRef().SetContent(parent->GetDataRef().GetContent() + content);
                    }
                    bool result = DeSerializeSnippet(snippet, treeNode, snippetType);
                    if (!result)
                    {
                        return false;
                    }
                }
            }
        }
    }
    return false;
}

std::wstring ParserXML::SerializeSnippet(std::shared_ptr<CatalogNode> node, int level) const
{
    SerializeNodeFile(node);

    std::wstringstream wss;
    if (node->GetNumChildren() > 0)
    {
        for (size_t i = 0; i < level; ++i)
        {
            wss << L" ";
        }
        wss << L"<" << node->GetData().GetName();
        wss << SerializeAttributes(node);
        wss << L">\n";
        for (node->Init(); auto child = node->Next();)
        {
            wss << SerializeSnippet(child, level + 1);
        }
        for (size_t i = 0; i < level; ++i)
        {
            wss << L" ";
        }
        wss << L"</" << node->GetData().GetName() << L">\n";
    }
    else
    {
        for (size_t i = 0; i < level; ++i)
        {
            wss << L" ";
        }
        wss << L"<" << node->GetData().GetName();
        wss << SerializeAttributes(node);

        wss << L"/>\n";
    }

    return wss.str();
}

#include <direct.h>

void ParserXML::DeSerializeNodeFile(std::shared_ptr<CatalogNode> node) const
{
    if (node->GetData().GetType() != ECatalogDataType::Dictionary)
    {
        return;
    }

    const DictionaryDataImpl* dictDataImpl = dynamic_cast<const DictionaryDataImpl*>(node->GetData().GetImpl());
    if (dictDataImpl)
    {
        std::wstring folderName = m_rootFileName.substr(0, m_rootFileName.find('.'));
        std::wstring nodeFileName = GetNodeFileName(node, folderName);
        std::wifstream ifs(nodeFileName);

        // To treat file content as wide characters:
        ifs.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

        if (ifs.is_open())
        {
            std::wstringstream wss;
            wss << ifs.rdbuf();

            // To allow wstring to be printed by wcout:
            //_setmode(_fileno(stdout), _O_U16TEXT);
            //std::wcout << wss.str();

            std::wstring wStr = wss.str();
            Snippet snippet(wStr, 0, wStr.size());
            DeSerializeSnippet(snippet, node, Snippet::ESnippetType::Dictionary);

            ifs.close();
        }
    }
}

void ParserXML::SerializeNodeFile(std::shared_ptr<CatalogNode> node) const
{
    if (node->GetData().GetType() != ECatalogDataType::Dictionary)
    {
        return;
    }

    const DictionaryDataImpl* dictDataImpl = dynamic_cast<const DictionaryDataImpl*>(node->GetData().GetImpl());
    if (dictDataImpl)
    {
        std::wstring folderName = m_rootFileName.substr(0, m_rootFileName.find('.'));
        int result = _wmkdir(folderName.c_str());

        std::wstring nodeFileName = GetNodeFileName(node, folderName);
        std::wofstream ofs(nodeFileName);

        // To treat file content as wide characters:
        ofs.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

        if (ofs.is_open())
        {
            std::wstringstream wss;
            int counter = 0;
            for (const auto& dictionaryPair : dictDataImpl->m_dictionary)
            {
                if (counter > 0)
                {
                    wss << "\n";
                }
                wss << "<word>" << dictionaryPair.first.c_str() << "</word>";
                wss << "<translation>" << dictionaryPair.second.c_str() << "</translation>";
                ++counter;
            }

            // For debug:
            //std::wcout << wss.str();

            ofs.write(wss.str().c_str(), wss.str().size());

            ofs.close();
        }
    }
}

std::wstring ParserXML::SerializeAttributes(std::shared_ptr<CatalogNode> node) const
{
    std::wstringstream wss;
    if (!node->GetData().GetTitle().empty())
    {
        wss << L" title=\"" << node->GetData().GetTitle() << L"\"";
    }
    if (node->GetData().HasType())
    {
        wss << L" type=\"" << CatalogData::CatalogDataTypeToWString(node->GetData().GetType()) << L"\"";
    }

    return wss.str();
}

//std::shared_ptr<CatalogNode> ParserXML::ConvertToNode(const Tag& tag, std::shared_ptr<CatalogNode> parent) const
//{
    //std::shared_ptr<CatalogNode> treeNode = DataManager::Instance()->CreateNode(tag.GetData(), parent);
    //DeSerializeNodeFile(treeNode);
    //return std::move(treeNode);
//}

void ParserXML::TagToDictionary(const Tag& tag, std::shared_ptr<CatalogNode> node) const
{
    if (node->GetData().GetType() != ECatalogDataType::Dictionary)
    {
        return;
    }

    DictionaryDataImpl* dictDataImpl = dynamic_cast<DictionaryDataImpl*>(node->GetData().GetImpl());
    if (dictDataImpl)
    {
        if (tag.GetData().GetName() == L"word")
        {
            dictDataImpl->m_dictionary.emplace_back(tag.GetData().GetContent(), L"");
        }
        else if (tag.GetData().GetName() == L"translation")
        {
            dictDataImpl->m_dictionary.back().second = tag.GetData().GetContent();
        }
    }
}

std::wstring ParserXML::GetNodeFileName(std::shared_ptr<CatalogNode> node, const std::wstring& folderName) const
{
    return std::move(folderName + L"/" + node->GetData().GetName() + L".txt");
}
