#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include "TreeNode.h"
#include "CatalogData.h"

using CatalogNode = TreeNode<CatalogData>;

#endif