#ifndef TREE_NODE_H
#define TREE_NODE_H

#include <vector>
#include <list>
#include <memory>
#include <string>
#include <functional>

template <typename T>
class TreeNode
{
public:
    using FuncType = std::function<void(const T&)>;

public:
    TreeNode(const T& data)
        : m_data(data)
        , m_pParent(nullptr)
        , m_spChildren()
        , m_wpReceivers()
        , m_iter(m_spChildren.begin())
    {
    }

    TreeNode(const TreeNode& other)
        : m_data(other.data)
        , m_pParent(nullptr)
        , m_spChildren()
        , m_wpReceivers()
        , m_iter(m_spChildren.begin())
    {
    }

    virtual ~TreeNode()
    {
    }

public:
    void AddChild(const std::shared_ptr<TreeNode>& childToAdd)
    {
        m_spChildren.push_back(childToAdd);
        m_spChildren.back()->m_pParent = this;
    }

    void RemoveChild(const std::shared_ptr<TreeNode>& childToRemove)
    {
        typename std::list<std::shared_ptr<TreeNode>>::iterator iter = std::find(m_spChildren.begin(), m_spChildren.end(), childToRemove);
        if (iter != m_spChildren.end())
        {
            m_spChildren.erase(iter);
        }
    }

    void MoveChild(const std::shared_ptr<TreeNode>& childToMove, bool forward)
    {
        typename std::list<std::shared_ptr<TreeNode>>::iterator iter = std::find(m_spChildren.begin(), m_spChildren.end(), childToMove);
        if (iter != m_spChildren.end())
        {
            auto iterTarget = iter;
            if (forward)
            {
                if (*iter == m_spChildren.back())
                {
                    return;
                }
                ++iterTarget;
            }
            else
            {
                if (iter == m_spChildren.begin())
                {
                    return;
                }
                --iterTarget;
            }
            std::swap(*iter, *iterTarget);
        }
    }

    void Init()
    {
        m_iter = m_spChildren.begin();
    }

    std::shared_ptr<TreeNode> Next()
    {
        return (m_iter != m_spChildren.end()) ? *m_iter++ : nullptr;
    }

    TreeNode* GetParent() const
    {
        return m_pParent;
    }

    const size_t GetNumChildren() const
    {
        return m_spChildren.size();
    }

    T& GetDataRef()
    {
        return m_data;
    }

    const T& GetData() const
    {
        return m_data;
    }

    void SetData(const T& data)
    {
        m_data = data;

        NotifyDataChanged(data);
    }

    void RegisterReceiver(std::shared_ptr<FuncType> spFunc)
    {
        std::weak_ptr<FuncType> wpFunc = spFunc;
        m_wpReceivers.push_back(wpFunc);
    }

    void UnregisterReceiver(std::shared_ptr<FuncType> spFunc)
    {
        for (auto iter = m_wpReceivers.begin(); iter != m_wpReceivers.end(); ++iter)
        {
            if (auto spReceiver = iter->lock())
            {
                if (spReceiver.get() == spFunc.get())
                {
                    m_wpReceivers.erase(iter);
                    break;
                }
            }
        }
    }

    void NotifyDataChanged(const T& data)
    {
        auto iter = m_wpReceivers.begin();
        while (iter != m_wpReceivers.end())
        {
            if (std::shared_ptr<FuncType> spReceiver = iter->lock())
            {
                (*spReceiver)(data);
                ++iter;
            }
            else
            {
                iter = m_wpReceivers.erase(iter);
            }
        }
    }

private:
    T m_data;
    TreeNode* m_pParent;
    std::list<std::shared_ptr<TreeNode>> m_spChildren;
    std::list<std::weak_ptr<FuncType>> m_wpReceivers;
    typename std::list<std::shared_ptr<TreeNode>>::iterator m_iter;
};

using StringNode = TreeNode<std::wstring>;

#endif
