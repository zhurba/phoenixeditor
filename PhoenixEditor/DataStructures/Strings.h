#ifndef STRINGS_H
#define STRINGS_H

const std::wstring strEmpty = L"";

const std::string strConfigFilePath = "Data/Config.txt";
const std::string strCatalogFilePath = "Data/Xml/Catalog.txt";
const std::string strIconAddFilePath = "Data/Images/ImageButtonAdd.png";
const std::string strIconDeleteFilePath = "Data/Images/ImageButtonDelete.png";

const std::wstring strAttrNameTitle = L"title";
const std::wstring strAttrNameType = L"type";

const std::wstring strCatalogDataTypeTree = L"tree";
const std::wstring strCatalogDataTypeMap = L"map";
const std::wstring strCatalogDataTypeText = L"text";
const std::wstring strCatalogDataTypeDictionary = L"dictionary";

const std::string strAddContextItem = "Add Item";
const std::string strAddAboveContextItem = "Add Item Above";
const std::string strAddBelowContextItem = "Add Item Below";
const std::string strDeleteContextItem = "Delete Item";

const std::wstring strNewCatalogItemName = L"Name";
const std::wstring strNewCatalogItemTitle = L"Title";

#endif