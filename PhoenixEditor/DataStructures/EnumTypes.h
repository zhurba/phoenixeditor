#ifndef ENUM_TYPES_H
#define ENUM_TYPES_H

enum class ECatalogDataType
{
    Graph = 0,
    Tree,
    Text,
    Dictionary,
    Count,
    None
};

#endif
