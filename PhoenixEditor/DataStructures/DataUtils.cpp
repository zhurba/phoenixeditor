#include <codecvt>

#include "DataUtils.h"

std::wstring DataUtils::QStringToWString(const QString& qString)
{
    const int stringSize = qString.size();
    wchar_t* wcArray = new wchar_t[static_cast<int64_t>(stringSize) + 1];
    wcArray[stringSize] = 0;
    qString.toWCharArray(wcArray);
    std::wstring wString(wcArray);
    delete[] wcArray;

    return std::move(wString);
}

std::string DataUtils::QStringToString(const QString& qString)
{
    return std::move(std::string(qString.toLocal8Bit().constData()));
}

QString DataUtils::WStringToQString(const std::wstring& wString)
{
     return std::move(QString::fromWCharArray(wString.c_str(), static_cast<int>(wString.size())));
}

QString DataUtils::StringToQString(const std::string& string)
{
    return std::move(QString::fromUtf8(string.c_str(), string.size()));
}

std::wstring DataUtils::StringToWString(const std::string& string)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return std::move(converter.from_bytes(string));
}

std::string DataUtils::WStringToString(const std::wstring& wString)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return std::move(converter.to_bytes(wString));
}
