#ifndef WIDGET_FACTORY_H
#define WIDGET_FACTORY_H

#include <string>

#include "DataTypes.h"
#include "EnumTypes.h"

class QWidget;
class CentralWidgetBase;
class IconWidgetBase;

class WidgetFactory
{
public:
    ~WidgetFactory();

public:
    static WidgetFactory* Instance();
    CentralWidgetBase* CreateCentralWidget(QWidget* parent, ECatalogDataType dataType);
    IconWidgetBase* CreateIconWidget(QWidget* parent, std::shared_ptr<CatalogNode> spData);

private:
    WidgetFactory();
    WidgetFactory(const WidgetFactory& other) = delete;
    WidgetFactory& operator=(const WidgetFactory& other) = delete;
};

#endif

