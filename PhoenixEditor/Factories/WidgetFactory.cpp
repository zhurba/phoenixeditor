#include "WidgetFactory.h"
#include "CentralWidgetGraph.h"
#include "CentralWidgetText.h"
#include "CentralWidgetDictionary.h"
#include "IconWidgetBase.h"

WidgetFactory::~WidgetFactory()
{
}

WidgetFactory* WidgetFactory::Instance()
{
    static WidgetFactory widgetFactory;
    return &widgetFactory;
}

CentralWidgetBase* WidgetFactory::CreateCentralWidget(QWidget* parent, ECatalogDataType dataType)
{
    CentralWidgetBase* centralWidget = nullptr;
    switch (dataType)
    {
    case ECatalogDataType::Graph:
        centralWidget = new CentralWidgetGraph(parent);
        break;
    case ECatalogDataType::Tree:
        centralWidget = new CentralWidgetBase(parent); // todo: implement CentralWidgetTree
        break;
    case ECatalogDataType::Text:
        centralWidget = new CentralWidgetText(parent);
        break;
    case ECatalogDataType::Dictionary:
        centralWidget = new CentralWidgetDictionary(parent);
        break;
    default:
        centralWidget = new CentralWidgetBase(parent);
    }
    return centralWidget;
}

IconWidgetBase* WidgetFactory::CreateIconWidget(QWidget* parent, std::shared_ptr<CatalogNode> spData)
{
    IconWidgetBase* iconWidget = new IconWidgetBase(parent, spData);
    return iconWidget;
}

WidgetFactory::WidgetFactory()
{
}